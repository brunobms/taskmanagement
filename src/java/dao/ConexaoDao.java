package dao;

/**
 *
 * @author Bruno Martins
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoDao {

    private static Connection conexao;

    public static Connection conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/taskManagement";
            String usuario = "brunobms";
            String senha = "bruno1234";
            conexao = DriverManager.getConnection(url, usuario, senha);
            System.out.println("Conexão estabelecida...");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("erro" + e);
        }
        return conexao;

    }

    public static void desconectar() {
        try {
            if (conexao != null) {
                conexao.close();
                System.out.println("Conexão fechada");
            }
        } catch (SQLException e) {
            System.out.println("Erro ao fechar a conexão" + e.getMessage());
        }
    }

}

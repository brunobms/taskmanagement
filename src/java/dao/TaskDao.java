package dao;

/**
 *
 * @author Bruno Martins
 */
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.TaskModel;

public class TaskDao {

    public boolean insere(TaskModel tarefa) {
        try {
            Connection c = new ConexaoDao().conectar();
            PreparedStatement statement = c.prepareStatement("insert into task (titulo, descricao, status, dataCriacao, dataTarefa) values (?, ?, ?, ?, ?)");
            statement.setString(1, tarefa.getTitulo());
            statement.setString(2, tarefa.getDescricao());
            statement.setString(3, tarefa.getStatus());
            statement.setDate(4, (Date) tarefa.getDataCriacao());
            statement.setDate(5, (Date) tarefa.getDataTarefa());
            statement.execute();

            return true;
        } catch (SQLException e) {
            System.out.println("Erro ao inserir registro " + e.getMessage());
            return false;
        }
    }

    public boolean alterar(TaskModel tarefa) {
        Connection c = new ConexaoDao().conectar();
        try {
            String query = "UPDATE task SET titulo=?, descricao=?, status=?, dataTarefa=?,  WHERE id=?";
            PreparedStatement statement = c.prepareStatement(query);

            statement.setString(1, tarefa.getTitulo());
            statement.setString(2, tarefa.getDescricao());
            statement.setString(3, tarefa.getStatus());
            statement.setDate(4, (Date) tarefa.getDataTarefa());
            statement.setInt(5, tarefa.getId());
            statement.executeUpdate();

            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            ConexaoDao.desconectar();
        }
    }

    public List<TaskModel> buscar() {
        List<TaskModel> lista = new ArrayList<TaskModel>();
        try {
            Connection conexao = ConexaoDao.conectar();
            PreparedStatement preparedStatement = conexao.prepareStatement("select * from task");
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                TaskModel um = new TaskModel();
                um.setId(rs.getInt("id"));
                um.setTitulo(rs.getString("titulo"));
                um.setDescricao(rs.getString("subtitulo"));
                um.setStatus(rs.getString("status"));
                um.setDataCriacao(rs.getDate("dataCriacao"));
                um.setDataTarefa(rs.getDate("dataTarefa"));
                lista.add(um);
            }
            ConexaoDao.desconectar();
        } catch (SQLException e) {
            System.out.println("Erro");
        }
        return lista;
    }

    public TaskModel buscarPorId(int id) {
        TaskModel model = new TaskModel();
        try {
            Connection conexao = new ConexaoDao().conectar();
            PreparedStatement preparedStatement = conexao.prepareStatement("select * from task WHERE id=?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                model.setId(rs.getInt("id"));
                model.setTitulo(rs.getString("titulo"));
                model.setDescricao(rs.getString("subtitulo"));
                model.setStatus(rs.getString("status"));
                model.setDataCriacao(rs.getDate("dataCriacao"));
                model.setDataTarefa(rs.getDate("dataTarefa"));
            }
            ConexaoDao.desconectar();
        } catch (SQLException e) {
            System.out.println("Erro");
        }

        return model;
    }

    public boolean apagar(TaskModel tarefa) {
        Connection c = new ConexaoDao().conectar();
        try {
            PreparedStatement statement = c.prepareStatement("delete from task WHERE id=" + tarefa.getId());
            statement.execute();
            statement.close();

            return true;
        } catch (SQLException e) {
            return false;
        } finally {
            ConexaoDao.desconectar();
        }

    }
}

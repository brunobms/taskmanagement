package bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.bean.ViewScoped;
import dao.TaskDao;
import model.TaskModel;
import saidas.Mensagens;

@ManagedBean
@ViewScoped
public class TaskBean implements Serializable{
    private TaskModel modelo = new TaskModel();
    private List<TaskModel> lista = new ArrayList<TaskModel>();
    private List<TaskModel> listaFiltro = new ArrayList<TaskModel>();
    private List<SelectItem> listaSelect;
     private TaskModel selecionado = new TaskModel();

   

    public TaskModel getModelo() {
        return modelo;
    }

    public void setModelo(TaskModel modelo) {
        this.modelo = modelo;
    }

    public List<TaskModel> getLista() {
        return lista;
    }

    public void setLista(List<TaskModel> lista) {
        this.lista = lista;
    }

    public List<TaskModel> getListaFiltro() {
        return listaFiltro;
    }

    public void setListaFiltro(List<TaskModel> listaFiltro) {
        this.listaFiltro = listaFiltro;
    }

    public List<SelectItem> getListaSelect() {
        if(this.listaSelect == null){
            listaSelect = new ArrayList<SelectItem>();
            
            for(TaskModel model : lista){
                SelectItem item = new SelectItem(model, model.getTitulo());
                
                this.listaSelect.add(item);
            }
            
        }
        return listaSelect;
    }

    public void setListaSelect(List<SelectItem> listaSelect) {
        this.listaSelect = listaSelect;
    }
    
    @PostConstruct
    public void verificaSelecionado(){
        selecionado = (TaskModel) FacesContext.
                getCurrentInstance().
                getExternalContext().
                getFlash().get("pselecionado");
        System.err.println("");
    }
    
    public void cadastrar(){
        if (selecionado == null) {
            if (new TaskDao().insere(modelo)) {
                modelo = new TaskModel();
                Mensagens.mostrarMensagemAviso("Cadastro realizado com sucesso", "Detalhes da mensagem");
            } else {
                Mensagens.mostrarMensagemErro("Cadastro não realizado", "Não foi possível realizar o cadastro");
            }
        }
        else {
            new TaskDao().alterar(selecionado);
            Mensagens.mostrarMensagemAviso("Registro atualizado com sucesso", "Detalhes da mensagem");
        }
    }
    
   

    public TaskModel getSelecionado() {
        return selecionado;
    }

    public void setSelecionado(TaskModel selecionado) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("pselecionado", selecionado);
        
        this.selecionado = selecionado;
    }
    
    public void apagar(TaskModel selecionado) {
        if (selecionado != null) {
            try {
                if (new TaskDao().apagar(selecionado)) {
                    lista.remove(selecionado);
                    System.out.println(lista.size());
                    Mensagens.mostrarMensagemAviso("Registro apagado", "Registro foi apagado com sucesso!" + selecionado);
                }
            } catch (Exception e) {
                Mensagens.mostrarMensagemErro("Erro", e.getMessage());
            }
        }
    }
    
}
